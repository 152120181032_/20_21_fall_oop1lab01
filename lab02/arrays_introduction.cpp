#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
#define size 10000

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT  */

    int A[size], N, i;
    cin >> N;

    for (i = 0; i < N; i++)
    {
        cin >> A[i];
    }

    for (i = N - 1; i >= 0; i--)
    {
        cout << A[i] << " ";
    }

    return 0;
}
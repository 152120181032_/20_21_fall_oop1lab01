#include<bits/stdc++.h>

using namespace std;
//Implement the class Box  
//l,b,h are integers representing the dimensions of the box

class Box {

    int l, br, h;

    // The class should have the following functions : 

    // Constructors: 
    // Box();
    // Box(int,int,int);
    // Box(Box);

public:
    Box() {
        l = 0;
        br = 0;
        h = 0;
    }
    Box(int new_l, int new_br, int new_h)
    {
        l = new_l;
        br = new_br;
        h = new_h;
    }
    Box(const Box& b) {
        l = b.l;
        br = b.br;
        h = b.h;

    }

    // int getLength(); // Return box's length
    // int getBreadth (); // Return box's breadth
    // int getHeight ();  //Return box's height
    // long long CalculateVolume(); // Return the volume of the box

    int getLength() {
        return l;
    }
    int getBreadth() {
        return br;
    }
    int getHeight() {
        return h;
    }
    long long CalculateVolume() {
        return (long long)l * br * h;
    }
    //Overload operator < as specified
    //bool operator<(Box& b)
    bool operator<(Box& b) {

        if (l < b.l)

        {
            return true;
        }
        else if (l == b.l)
        {
            if (br < b.br)
            {
                return true;
            }
            else if (br == b.br)
            {
                if (h < b.h)
                {
                    return true;
                }
            }
        }
        return false;
    }

};

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)

ostream& operator << (ostream& out, Box& B)
{
    out << B.getLength() << " " << B.getBreadth() << " " << B.getHeight();
    return out;
};

void check2()
{
    int n;
    cin >> n;
    Box temp;
    for (int i = 0; i < n; i++)
    {
        int type;
        cin >> type;
        if (type == 1)
        {
            cout << temp << endl;
        }
        if (type == 2)
        {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            temp = NewBox;
            cout << temp << endl;
        }
        if (type == 3)
        {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            if (NewBox < temp)
            {
                cout << "Lesser\n";
            }
            else
            {
                cout << "Greater\n";
            }
        }
        if (type == 4)
        {
            cout << temp.CalculateVolume() << endl;
        }
        if (type == 5)
        {
            Box NewBox(temp);
            cout << NewBox << endl;
        }

    }
}

int main()
{
    check2();
}
#include <stdlib.h>
#include <iostream>
using namespace std;

void update(int* a, int* b) {
    // Complete this function   
    cout << (*a + *b) << endl;
    cout << abs(*a - *b);
}

int main() {
    int a, b;
    int* pa, * pb;

    cin >> a >> b;
    pa = &a;
    pb = &b;

    update(pa, pb);

    return 0;
}

#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code.
    int i;
    long int l;
    char c;
    float f;
    double d;

    cin >> i >> l >> c >> f >> d;
    cout << i << endl << l << endl << c << endl;
    printf("%.3f\n", f);
    printf("%.9f", d);
    
    return 0;
}